from django.db import models

# Create your models here.
class Project(models.Model):
    title= models.CharField(max_length=200,  verbose_name="Título")
    description = models.TextField(verbose_name="Descripción breve", max_length=250)
    content = models.TextField(verbose_name="Contenido completo del blog", default='Contenido básico del blog')
    image = models.ImageField(verbose_name="Imagenes", upload_to="projects")
    created = models.DateTimeField(auto_now_add=True, verbose_name="Fecha de Creación")
    updated = models.DateTimeField(auto_now=True, verbose_name="Fecha de Edición")
    
    #cambiamos el nombre de la app y la ordenamos del mas nuevo al mas antiguo
    class Meta:
        verbose_name="Publicacion"
        verbose_name_plural="Publicaciones"
        ordering=["-created"]

    #cambiar el nombre que aparece en el listado
    def __str__(self):
        return self.title
