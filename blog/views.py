from django.shortcuts import render, get_object_or_404
from .models import Project
from .forms import ContactForm
from django.conf import settings
from django.core.mail import send_mail
# Create your views here.

def index(request):
    projects=Project.objects.all()
    contact_form=ContactForm(request.POST or None)
    if contact_form.is_valid():
        form_email = contact_form.cleaned_data.get("email")
        form_nombre = contact_form.cleaned_data.get("name")
        form_mensaje = contact_form.cleaned_data.get("content")
        form_asunto = contact_form.cleaned_data.get("subject")
        email_from = settings.EMAIL_HOST_USER
        email_to = ["jurgenhuerlo25@gmail.com"]
        email_mensaje = "%s:\n %s \n\nEnviador por:  %s " %(form_nombre,form_mensaje,form_email)
        asunto=form_asunto
        send_mail(asunto,
            email_mensaje,
            email_from,
            email_to,
            fail_silently = False
        )
    context = {
        'projects':projects,
        'form':contact_form 
    }
    return render(request,"blog/index.html",context)

def single(request, single_id):
    content=get_object_or_404(Project, id=single_id)
    context = {
        'content':content,
    }
    return render(request, "blog/single.html", context)
